package cn.tycoding.controller.admin;

import cn.tycoding.entity.Result;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Encapsulates the control layer for file upload and download
 *
 * @auther Hoos
 * @date 2018/8/2
 */
@RestController
public class UploadDownController {

    /**
     * File Upload
     * @param picture
     * @param request
     * @return
     */
    @RequestMapping("/upload")
    public Result upload(@RequestParam("picture") MultipartFile picture, HttpServletRequest request) {
        //Get the file in the server's storage location
        String path = request.getSession().getServletContext().getRealPath("/upload");
        File filePath = new File(path);
        System.out.println("File save path：" + path);
        if (!filePath.exists() && !filePath.isDirectory()) {
            System.out.println("Directory does not exist, create directory" + filePath);
            filePath.mkdir();
        }

        //Get the original file name (including format)
        String originalFileName = picture.getOriginalFilename();
        System.out.println("Original file name:" + originalFileName);

        //Get the file type, with the last `.` as the identifier
        String type = originalFileName.substring(originalFileName.lastIndexOf(".") + 1);
        System.out.println("File type:" + type);
        //Get the file name (without formatting)
        String name = originalFileName.substring(0, originalFileName.lastIndexOf("."));

        //Set file new name: current time + file name (without format)
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String date = sdf.format(d);
        String fileName = date + name + "." + type;
        System.out.println("New file name:" + fileName);

        //Create a file under the specified path
        File targetFile = new File(path, fileName);

        //Save the file to the specified location on the server
        try {
            picture.transferTo(targetFile);
            System.out.println("Upload success");
            //Return the file to the server's storage path
            return new Result(true,"/upload/" + fileName);
        } catch (IOException e) {
            System.out.println("upload failed");
            e.printStackTrace();
            return new Result(false, "upload failed");
        }
    }
}
