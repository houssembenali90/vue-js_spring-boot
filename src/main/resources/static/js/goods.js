/**
 * Off-campus customer information list
 */

//Set the global form submission format
Vue.http.options.emulateJSON = true;

// Vue instance
var vm = new Vue({
    el: '#app',
    data() {
        return {
            //The parameters required by the element-ui table must be of type Array.
            goods: [{
                id: '',
                title: '',
                price: '',
                image: '',
                brand: ''
            }],

            //edit table
            editor: {
                title: '',
                price: '',
                image: '',
                brand: ''
            },
            //add dialog
            showSave: false,
            //edit dialog
            showEditor: false,

            //Conditional query separately wrapped object
            searchEntity: {},

            //All data in the row selected by checkbox will be placed in the multipartSelection array
            multipleSelection: [],
            selectIds: [], //The id value selected by the checkbox for bulk deletion
            count: 0, //Tag column, this item is a few lines of checkbox selection

            //Paging option
            pageConf: {
                //Set some initial values (will be overwritten)
                pageCode: 1, //current page
                pageSize: 6, //Number of records displayed per page
                totalPage: 12, //total
                pageOption: [6, 10, 20], //Paging option
            },

            loading: {},

            //File upload parameters
            dialogImageUrl: '',
            dialogVisible: false,
            //Picture list (for echoing pictures)
            fileList: [{name: '', url: ''}],

            activeIndex: '2', //Default activation
        }
    },
    methods: {
        /**
         * loading animation
         */
        loadings() {
            this.loading = this.$loading({
                lock: true,
                text: 'Desperately loading',
                spinner: 'el-icon-loading',
            });
            setTimeout(() => {
                this.loading.close();
            }, 2000);
        },

        /**
         * Public method
         */
        //refresh the list
        reloadList() {
            console.log("totalPage:" + this.pageConf.totalPage + ",pageSize:" + this.pageConf.pageSize + ",:pageCode:" + this.pageConf.pageCode);
            this.search(this.pageConf.pageCode, this.pageConf.pageSize);
        },
        //Conditional query
        search(pageCode, pageSize) {
            this.loadings();
            this.$http.post('/goods/findByConPage?pageSize=' + pageSize + '&pageCode=' + pageCode, this.searchEntity).then(result => {
                console.log(result);
                this.goods = result.body.rows;
                this.pageConf.totalPage = result.body.total;
                this.loading.close(); //Manually close the animation if the data is updated successfully
            });

        },
        //checkbox
        selectChange(val) {
            this.count = val.length;
            this.multipleSelection = val;
        },
        //Empty selected
        clearSelect() {
            this.$refs.goods.clearSelection();
        },
        //Function triggered when pageSize changes
        handleSizeChange(val) {
            this.search(this.pageConf.pageCode, val);
        },
        //The function that is triggered when the current page changes
        handleCurrentChange(val) {
            this.pageConf.pageCode = val; //In order to ensure that the page is still on the current page after refreshing the list, instead of jumping to the first page
            this.search(val, this.pageConf.pageSize);
        },
        //Update
        sureEdit(editor) {
            //Close dialog
            this.showEditor = false;
            //Call the interface to update data
            this.$http.post('/goods/update', JSON.stringify(this.editor)).then(result => {
                if (result.body.success) {
                    //update completed
                    this.$message({
                        type: 'success',
                        message: result.body.message,
                        duration: 6000
                    });
                    //refresh the list
                    this.reloadList();
                    this.goods = [];
                    this.$refs.editor.resetFields();
                } else {
                    //Update failed
                    this.$message({
                        type: 'warning',
                        message: result.body.message,
                        duration: 6000
                    });
                    //refresh the list
                    this.reloadList();
                    this.$refs.editor.resetFields();
                }
            })
        },
        //delete
        sureDelete(ids) {
            this.$confirm('Are you sure to permanently delete this user information?', 'prompt', {
                confirmButtonText: 'determine',
                cancelButtonText: 'cancel',
                type: 'warning',
                center: true
            }).then(() => {
                //Call the deleted interface (the data must be converted to JSON format, otherwise the value will not be received, and the backend should be marked with @RequestBody annotation)
                this.$http.post('/goods/delete', JSON.stringify(ids)).then(result => {
                    if (result.body.success) {
                        //successfully deleted
                        this.selectIds = []; //Clear option
                        this.$message({
                            type: 'success',
                            message: result.body.message,
                            duration: 6000
                        });
                        //refresh the list
                        //Why judge and assign?
                        //A: Even if reloadList() is called to refresh the list, for delete, the totalPage total record and pageCode current page obtained in reloadList() are the records before deletion. When the last record of this page is deleted, the page number Will automatically jump to the previous page, but the data in the table shows "no record"
                        //   So to judge, if it is to delete the last record of this page, automatically delete to the previous page after deletion, the data is also the data of the previous page
                        if ((this.pageConf.totalPage - 1) / this.pageConf.pageSize === (this.pageConf.pageCode - 1)) {
                            this.pageConf.pageCode = this.pageConf.pageCode - 1;
                        }
                        this.reloadList();
                    } else {
                        //failed to delete
                        this.selectIds = []; //Clear option
                        this.$message({
                            type: 'warning',
                            message: result.body.message,
                            duration: 6000
                        });
                        //refresh the list
                        this.reloadList();
                    }
                });
            }).catch(() => {
                this.$message({
                    type: 'info',
                    message: 'Undelete',
                    duration: 6000
                });
            });
        },

        //add
        save(editor) {
            this.$refs[editor].validate((valid) => {
                if (valid) {
                    //shut down dialog
                    this.showSave = false;
                    //Call the saved interface
                    this.$http.post('/goods/create', JSON.stringify(this.editor)).then(result => {
                        if (result.body.success) {
                            //Successfully saved
                            this.$message({
                                type: 'success',
                                message: result.body.message,
                                duration: 6000
                            });
                            //Refresh form
                            this.reloadList();
                            this.editor = {};
                            this.$refs.editor.resetFields();
                        } else {
                            //Save failed
                            this.$emit(
                                'save',
                                this.$message({
                                    type: 'warning',
                                    message: result.body.message,
                                    duration: 6000
                                }),
                            );
                            //Refresh form
                            this.reloadList();
                            this.editor = {};
                            this.$refs.editor.resetFields();
                        }
                    });
                } else {
                    this.$emit(
                        'save',
                        this.$message({
                            message: 'The input information is incorrect!',
                            type: 'warning',
                            duration: 6000
                        }),
                    );
                    return false;
                }
            });
        },

        /**
         * Private method
         */
        //Add button
        saveBtn() {
            //Open new dialog
            this.showSave = true;
            this.editor = {}; //Clear form
            this.fileList = []; //Clear file list
            //Clear raw data
            if (this.$refs['editor'] !== undefined) {
                this.$refs['editor'].resetFields(); //Query: It may be because the object has not been generated, resulting in misreading the empty object and reporting an error.
            }
        },
        //Update button (table)
        handleEdit(id) {
            //turn on dialog
            this.showEditor = true;
            this.editor = {}; //Clear form
            //Query the data corresponding to the current id
            this.$http.post('/goods/findById', {id: id}).then(result => {
                this.fileList.forEach(row => {
                    row.url = result.body[0].image; //Assign the URL address of the image to the file-list to display it.
                });
                this.editor = result.body[0];
                //Remove element-ui form checksum residue
                this.$refs['editor'].resetFields();
            });

            console.log(this.editor);
            console.log(this.fileList);
        },
        //Update button（checkbox）
        editSelect() {
            if (this.multipleSelection.length === 1) {
                this.multipleSelection.forEach(row => {
                    console.log(row);
                    this.editor = {}; //Clear data first
                    this.editor = row; //Assignment
                    this.fileList.forEach(file => {
                        file.url = row.image; //Assign the URL address of the image to the file-list to display it.
                    });
                    //Remove element-ui form checksum residue
                    this.$refs['editor'].resetFields();
                    this.showEditor = true; //Open dialog
                });
            } else if (this.multipleSelection.length > 1) {
                //Only one can be selected for editing
                this.$message({
                    type: 'info',
                    message: 'Please choose any one to edit',
                    duration: 6000
                });
            } else {
                this.$message({
                    type: 'info',
                    message: 'Please select at least one to edit',
                    duration: 6000
                })
            }
        },
        //Delete button
        handleDelete(id) {
            var ids = new Array();
            ids.push(id);
            this.sureDelete(ids);
        },
        //Bulk delete button（checkbox）
        deleteSelect(rows) {
            if (rows) {
                rows.forEach(row => {
                    this.selectIds.push(row.id);
                    this.$refs.goods.toggleRowSelection(row);
                });
                //Call delete method
                this.sureDelete(this.selectIds);
            } else {
                this.$refs.goods.clearSelection();
            }
        },

        /**
         * upload picture
         * @param res
         * @param file
         * @param fileList
         */
        //Hook function for successful file upload
        handleSuccess(res, file, fileList) {
            this.$message({
                type: 'info',
                message: 'Image uploaded successfully',
                duration: 6000
            });
            if (file.response.success) {
                this.editor.image = file.response.message; //Assign the returned file storage path to the image field
            }
        },
        //Hook function before deleting a file
        handleRemove(file, fileList) {
            console.log(file, fileList);
            this.$message({
                type: 'info',
                message: 'Deleted original image',
                duration: 6000
            });
        },
        //Click on the hook function of the uploaded file in the list
        handlePreview(file) {
            // this.dialogImageUrl = file.url;
            // this.dialogVisible = true;

        },
        //The function that is triggered when the number of uploaded files exceeds the setting
        onExceed(files, fileList) {
            this.$message({
                type: 'info',
                message: 'Can only upload at most one image',
                duration: 6000
            });
        },
        //The hook function before the file upload
        //The parameter is the uploaded file. If it returns false, or returns to Primary and is rejected, stop uploading.
        beforeUpload(file) {
            const isJPG = file.type === 'image/jpeg';
            const isGIF = file.type === 'image/gif';
            const isPNG = file.type === 'image/png';
            const isBMP = file.type === 'image/bmp';
            const isLt2M = file.size / 1024 / 1024 < 2;

            if (!isJPG && !isGIF && !isPNG && !isBMP) {
                this.$message.error('Uploading images must be in JPG/GIF/PNG/BMP format!');
            }
            if (!isLt2M) {
                this.$message.error('Upload image size can''t exceed 2MB!');
            }
            return (isJPG || isBMP || isGIF || isPNG) && isLt2M;
        },

    },

    // Life cycle function
    created() {
        // this.findAll();
        this.search(this.pageConf.pageCode, this.pageConf.pageSize);
        this.loadings(); //Loading animation
    },

});
