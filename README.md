# Vous apprend une introduction élégante à Spring Boot2.x

**pile technologique**

* Backend: SpringBoot2.x + Mybatis
* Frontend: Vue.JS2.x + ElementUI

**Environnement de test**

* IDEA + SpringBoot-2.0.5

**Instructions de démarrage**

* Avant de commencer, veuillez configurer le nom d'utilisateur et le mot de passe pour vous connecter à la base de données dans [application.yml] (https://github.com/TyCoding/spring-boot/blob/master/src/main/resources/application.yml), et Adresse et informations de port pour le serveur Redis.

* Avant de commencer, veuillez créer la base de données `seckill`, l'instruction SQL est intégrée dans: [/db/sys_schema.sql] (https://gitlab.com/houssembenali90/vue-js_spring-boot/blob/master/db/sys_schema.sql). Veuillez vous reporter au fichier SQL pour des instructions de construction spécifiques.

* Une fois la configuration terminée, exécutez la méthode main dans SpringbootApplication sous `src/main/cn/tycoding/` et accédez à `http://localhost:8080/` pour le test de l'API.

**Conception du projet**

```
.
├── README
├── README.md
├── db
├── mvnw
├── mvnw.cmd
├── pom.xml
├── spring-boot.iml
├── src
│   ├── main
│   │   ├── java
│   │   │   └── cn
│   │   │       └── tycoding
│   │   │           ├── SpringbootApplication.java  -- Classe lanceur Spring Boot
│   │   │           ├── controller  -- Couche MVC-WEB
│   │   │           ├── entity  -- Classe d'entité
│   │   │           ├── interceptor  -- Intercepteur personnalisé
│   │   │           ├── mapper  -- mybatis-Mapper Interface de mappage de couche ou couche DAO
│   │   │           └── service  -- service Couche métier
│   │   └── resources  -- Spring Boot Répertoire du fichier de ressources
│   │       ├── application.yml  -- Spring Boot Fichier de configuration principal
│   │       ├── mapper  -- Mybatis Mapper Fichier de configuration XML de couche  
│   │       ├── static  -- Fichiers statiques frontaux (principalement JS, CSS, fichiers d'image, ne mettent généralement pas de pages HTML)
│   │       │   ├── css
│   │       │   ├── image
│   │       │   ├── js
│   │       │   ├── lib
│   │       └── templates  -- Thymeleaf Répertoire  de pages HTML reconnu par le moteur de modèle, qui stocke la page HTML (équivalent du répertoire WEB-INF précédent, auquel il est impossible d'accéder directement via le navigateur).
│   └── test
```
# Préparation

Démarrez le projet Spring Boot réel. Tout d’abord, vous devez créer le projet Spring Boot.

Création du projet Spring Boot, veuillez consulter ce blog: [Construction du projet d’entrée Spring Boot] (http://tycoding.cn/2018/09/28/spring-boot/)

## Spring Boot Launcher

Spring Boot fournit un grand nombre de lanceurs d’applications pour prendre en charge différentes fonctions. Pour parler franchement, c’est la configuration de dépendance dans `pom.xml`. En raison des fonctions de configuration automatique de Spring Boot, il n’est pas nécessaire de prendre en compte la version de dépendance du projet. Le programme de lancement d'applications de Spring Boot, qui nous aide automatiquement à importer toutes les dépendances pertinentes dans le projet.

Voici quelques lanceurs d'applications courants:

* `spring-boot-starter`: le programme de lancement principal de Spring Boot, qui inclut la configuration automatique, la journalisation et YAML
* `spring-boot-starter-aop`: prend en charge les fonctionnalités de programmation orientées visage d'AOP, y compris Spring-aop et AspecJ
* `spring-boot-starter-cache`: prend en charge l'abstraction du cache de Spring
* `spring-boot-starter-artermis`: Prend en charge l'API JMS (Java Message Service) via Apache Artemis
* `spring-boot-starter-data-jpa`: prenez en charge JPA
* `spring-boot-starter-data-solr`: prend en charge la plate-forme de recherche Apache Solr, y compris spring-data-solr
* `spring-boot-starter-freemarker`: moteur de template FreeMarker
* `spring-boot-starter-jdbc`: prend en charge la base de données JDBC
* `spring-boot-starter-Redis`: Prend en charge la base de données de stockage de clés-valeur Redis, y compris spring-redis
* `spring-boot-starter-security`: supporte Spring-security
* `spring-boot-starter-thymeleaf`: Prend en charge le moteur de modèle Thymeleaf, y compris l'intégration à Spring
* `spring-boot-starter-web`: prend en charge le développement Web en pile complète, y compris tomcat et Spring-WebMVC
* `spring-boot-starter-log4j`: Support de la structure de journalisation Log4J
* `spring-boot-starter-logging`: Présentation de la structure de journalisation par défaut de Spring Boot

## Conception de la structure du projet Spring Boot

Le projet Spring Boot (projet Maven) a bien entendu la structure de base du projet Maven. Autre que ceci:

1. Le répertoire webapp (webroot) n'est pas inclus dans le projet Spring Boot.
2. Le répertoire de ressources statiques fourni par Spring Boot par défaut doit être placé sous le chemin de classe et son nom de répertoire doit respecter certaines règles.
3. Spring Boot ne préconise pas l'utilisation de fichiers de configuration XML par défaut et préconise l'utilisation de YML en tant que format de fichier de configuration, dont la syntaxe est plus concise. Bien entendu, vous pouvez également utiliser .properties comme format de fichier de configuration.
4. Spring Boot recommande officiellement d'utiliser Thymeleaf en tant que moteur de gabarit frontal et Thymeleaf utilise par défaut gabarits comme répertoire de stockage pour les pages statiques (spécifiées par les fichiers de configuration).
5. Spring Boot utilise par défaut `resources` comme répertoire de stockage pour les ressources statiques et stocke les fichiers statiques frontaux et les fichiers de configuration du projet.

6. Spring Boot stipule que le nom du sous-répertoire sous `resources` doit être conforme à certaines règles. En règle générale, nous définissons `resources/static` dans le répertoire de stockage statique frontal (JS, CSS); définissons `resources/templates` comme stockage de pages HTML. Répertoire.

7. Le répertoire de fichiers du moteur de modèle Thymeleaf `/ resources/templates` spécifié par Spring Boot est un répertoire protégé. Il est similaire au dossier WEB-INF du projet WEB précédent. Il permet de ne pas accéder directement aux ressources statiques. Accès via la carte du contrôleur.

8. Il est recommandé de placer le fichier de mappage XML Mybatis-Mapper dans le répertoire `resources/`. Je le place dans le répertoire `resources/mapper` et l'interface de couche Mapper sous `src/main/java/Mapper` devrait utiliser `@ Les annotations du mappeur, sinon mybatis ne trouve pas le fichier de mappage XML correspondant à l'interface.

9. `SpringBootApplication.java` est la classe du programme de lancement du projet. Il n'est pas nécessaire de déployer le projet sur Tomcat, le projet de déploiement du serveur fourni par SpringBoot (exécutez la classe du programme de lancement) et SpringBoot analysera automatiquement les homologues du programme de lancement et les enfants. Les beans marqués avec des annotations, c'est-à-dire le `<contexte: composant-scan>` que nous avons configuré précédemment, ne nécessitent pas une configuration similaire dans SpringBoot.

10. Spring Boot ne recommande pas d'utiliser les pages JSP. Si vous souhaitez l'utiliser, veuillez utiliser la solution Baidu. Nous utilisons le moteur de template page + Thymeleaf HTML. Le moteur de modèles Thyemeleaf fournit de nombreuses syntaxes intégrées, telles que: `` div th: text = "$ {xx}"> `peut être utilisé pour récupérer les données stockées dans l'objet de domaine.

11. Spring Boot mentionné ci-dessus fournit un répertoire pour stocker les pages statiques HTML.Le `resources/templates` est un répertoire protégé. Les pages HTML utilisées sont mappées via le contrôleur. Ceci indique indirectement que vous devez configurer le résolveur de vues de Spring. Et la classe Controller ne peut pas utiliser l'identifiant `@ RestController`.


# Début

* Premièrement: ***Je tiens à souligner que SpringBoot n'est pas une amélioration de la fonctionnalité Spring, mais plutôt un moyen d'utiliser rapidement Spring**. Assurez-vous de vous en souvenir.

Apprendre à utiliser le framework SpringBoot n'a pour but que de faciliter son utilisation.Les connaissances acquises lors de la phase SSM sont maintenant pleinement applicables au framework Spring Boot.La majeure partie de notre apprentissage concerne la configuration automatisée de SpringBoot.

Le gros avantage du framework Spring Boot étant sa configuration automatisée, la configuration de pom.xml permet de le voir clairement.

Je recommande donc ici mon précédent projet d’intégration de phase SSM: [Cas d’intégration d’entrée détaillée SSM] (https://github.com/TyCoding/ssm) [SSM + Redis + Shiro + Projet d’intégration Solr + Vue.js] (https: //github.com/TyCoding/ssm-redis-solr)

## Dépendance du projet

Les dépendances de ce projet se trouvent dans le référentiel Github: [spring-boot/pom.xml] (https://github.com/TyCoding/spring-boot/tree/master/pom.xml)

## Initialisation de la base de données

Veuillez vous reporter au référentiel GitHub pour la conception de la table de base de données de ce projet: [spring-boot/db /] (https://github.com/TyCoding/spring-boot/tree/master/db)

Veuillez exécuter la structure de la table de projet avant d'exécuter le projet.

## Intégration SpringBoot Mybatis

Comme je l'ai dit précédemment:**Le cadre SpringBoot n'est pas une amélioration de la fonctionnalité Spring, mais constitue un moyen d'utiliser rapidement Spring**.

Donc, l’idée d’intégration de Mybatis par SpringBoot est fondamentalement la même que celle de Mybatis pour Spring, il y a deux différences:

* 1. Le fichier de configuration XML de l'interface du mappeur change. Auparavant, nous utilisions le développement d'agent d'interface Mybatis, qui spécifie que le fichier de mappage Mapper doit se trouver dans un répertoire avec l'interface; le fichier de mappage Mapper est ici placé sous «resources/mapper /» et placé dans l'interface Mapper sous «src/main/java/` Vous devez annoter l'identifiant avec `@ Mapper`, sinon le fichier de mappage et l'interface ne peuvent pas correspondre.

* 2. SpringBoot recommande d'utiliser YAML en tant que fichier de configuration, ce qui simplifie la configuration. Donc, l'intégration de Mybatis a une certaine différence dans le fichier de configuration, mais au final, c'est la configuration de ces paramètres.

Veuillez vous référer à Baidu pour la grammaire de YAML Je ne remplis que les exigences de configuration de base et ne comporte pas de grammaire difficile à comprendre.

### Profil d'intégration

Pour le code détaillé dans cet exemple, veuillez consulter le référentiel GitLab: [spring-boot/resources/application.yml] (https://github.com/TyCoding/spring-boot/tree/master/src/main/resources/application.yml)

La configuration de mybatis avec XML dans la phase de spring n’est rien d’autre que de la configuration: 1. pool de connexions, 2. connexion aux bases de données, 3. pilote mysql, 4. autres configurations d’initialisation

```YAML
    spring:
  datasource:
    name: springboot
    type: com.alibaba.druid.pool.DruidDataSource
    #druid Configuration connexe
    druid:
      # Filters Filtres interceptés statistiquement
      filter: stat
      #Driver mysql 
      driver-class-name: com.mysql.jdbc.Driver
      # Attribut de base
      url: jdbc:mysql://127.0.0.1:3306/springboot?useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true
      username: root
      password: root
      #Configure Initialization Size/Min/Max
      initial-size: 1
      min-idle: 1
      max-active: 20
      #Obtenir le délai d'attente de connexion
      max-wait: 60000
      #Combien de temps faut-il pour détecter la connexion inactive à fermer?
      time-between-eviction-runs-millis: 60000

  #mybatis Configuration
  mybatis:
    mapper-locations: classpath:mapper/*.xml
    type-aliases-package: cn.tycoding.entity
```

**Remarque: les espaces représentent les hiérarchies de noeuds, les commentaires sont marqués avec `#`**

**Explication**

1. Nous implémentons l'intégration de spring-mybatis, y compris la configuration de mybatis et la configuration de la source de données de la source de données, qui fait bien sûr partie de la configuration de spring, elle doit donc être placée sous `spring:`.

2. `mapper-locations` est équivalent au `<property name = "mapperLocations">` en XML pour analyser le fichier de configuration de la couche mappeur. Comme notre fichier de configuration se trouve sous `resources`, nous devons spécifier `classpath:`.

3. `type-aliases-package` est assez similaire à la configuration de l'alias `<property name = "typeAliasesPackase">` en XML, prenant généralement le nom de la classe d'entité sous celle-ci comme un alias.

4. `datasource` La configuration de la source de données, `name` indique le nom de la source de données actuelle, similaire au précédent attribut `<bean id =" dataSource ">` id, qui peut être spécifié de manière arbitraire ici, car nous n'avons pas besoin de faire attention à la manière dont Spring est injecté. Cet objet Bean.

5. `druid` représente l'utilisation du pool de connexions druid d'Ali dans ce projet, `driver-class-name:` est équivalent à `<property name="driverClassName">` ; `url`  en XML représente en `<property name="url">`; `username` représente le `<property name ="username">`; `password` en XML représentant le `<property name="password">` en XML; la propriété privée d'autres druides La configuration n'est plus expliquée. Notez ici que le pool de connexions druid et le pool de connexions c3p0 diffèrent par le nom de la <propriété> XML et que le nom de la configuration SpringBoot est différent ici.


Si Spring est familiarisé avec la configuration de Mybtis, vous connaissez cette configuration et il est facile de la distinguer du nom anglais. Il convient de noter ici que la syntaxe YAML stipule que différentes lignes d'espaces représentent différentes structures hiérarchiques.

Maintenant que nous avons terminé la configuration de base de SpringBoot-Mybatis, voyons comment implémenter CRUD de base.

### Implémentation de la requête

> 1. Créez une nouvelle classe d'entités `User.java` sous` src/main/java/cn/tycoding/entity/`

```java
public class User implements Serializable {
    private Long id; // numéro
    private String username; // nom d'utilisateur
    private String password; // mot de passe
    // getter/setter
}
```

> 2. Créez une interface générique `BaseService.java` sous` src/main/java/cn/tycoding/service/` pour simplifier l'écriture de méthodes CRUD de base pour l'interface de couche de service.

```java
public interface BaseService <T> {

    // interroger tout
    List<T> findAll ();

    // Selon la requête d'ID
    List<T> findById (Long id);

    // Ajouter
    void create(T t);

    // delete (batch)
    void delete(Long... ids);

    // Modifier
    void update(T t);
}
```

Ce qui précède est ma simple encapsulation de l’interface CRUD de base de la couche Service: j’utilise la classe générique et l’interface d’héritage spécifie quel type générique et T représente quelle classe.

> 3. Créez l'interface `UserService.java` sous `src/main/java/cn/tycoding/service/`:

```java
public interface UserService extends BaseService<User> {}
```

> 4. Créez la classe d'implémentation `UserServiceImpl.java` sous `src/main/java/cn/tycoding/service/impl/`:

```java
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> findAll() {
        return userMapper.findAll();
    }
    
    // Les autres méthodes sont omises
}
```

> 5. Créez la classe d’interface `UserMapper.java`Mapper sous `src/main/java/cn/tycoding/mapper/`:

```java
@Mapper
public interface UserMapper {
    List<User> findAll();
}
```

Comme ci-dessus, nous devons utiliser l'interface `@ Mapper` pour identifier cette interface, sinon Mybatis ne peut pas trouver le fichier de mappage XML correspondant.
> 6. Créez un fichier de mappage `UserMapper.xml` sous `src/main/resources/mapper/`:

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="cn.tycoding.mapper.UserMapper">

    <!-- Interroger tout -->
    <select id="findAll" resultType="cn.tycoding.entity.User">
        SELECT * FROM tb_user
    </select>
</mapper>
```

> 7. Créez `UserController.java` sous `src/main/java/cn/tycoding/controller/admin/`

```java
@RestController
public class UserController {
    @Autowired
    private UserService userService;
    
    @RequestMapping("/findAll")
    public List<User> findAll() {
        return userService.findAll();
    }
}
```

> 8. Exécutez la méthode principale `src/main/java/cn/tycoding/SpringbootApplication.java` pour lancer springboot

Vous pouvez obtenir un tas de données JSON en accédant à `localhost: 8080/findAll` sur votre navigateur.


### Penser

J'ai vu le pas à pas ci-dessus. Vous devez comprendre que c’est fondamentalement la même chose que CRUD dans la phase SSM, je ne donnerai pas d’exemples d’autres méthodes ici.

Ici, nous expliquons les différents endroits:

## Implémenter un saut de page

Le répertoire `src/main/resources/templates/` spécifié par Thymeleaf étant un répertoire protégé, il est impossible d'accéder directement aux ressources qui s'y trouvent via le navigateur. Vous pouvez utiliser la méthode de mappage du contrôleur pour y accéder. Comment le mapper?

> 1. Ajouter une configuration dans application.yml

```yaml
spring:
  thymeleaf:
    prefix: classpath:/templates/
    check-template-location: true
    suffix: .html
    encoding: UTF-8
    mode: LEGACYHTML5
    cache: false
```

Spécifie que le moteur de modèle Thymeleaf analyse les fichiers se terminant par `.html` dans le dossier` templates` sous `ressources`. Ceci implémente la configuration du résolveur de vue dans MVC:

```xml
    <! - Configurer View Parser ->
    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="prefix" value="/"/>
        <property name="suffix" value=".jsp"/>
    </bean>
```

Est-ce pratique de se sentir beaucoup? Cependant, il convient de noter ici que l'adresse du répertoire après `classpath:` doit être précédée de `/`, telle que le `classpath:/templates/` actuel.

> 2. Ajouter une méthode de mappage dans le contrôleur

```java
    @GetMapping(value = {"/", "/index"})
    public String index() {
        return "home/index";
    }
```

Ainsi, accéder à `localhost:8080/index` vous mènera directement à la page `resources/templates/home/index.html`.


## Implémentation de requêtes paginées

Nous devons d’abord configurer le plugin pageHelper dans application.yml

```yaml
pagehelper:
  pagehelperDialect: mysql
  reasonable: true
  support-methods-arguments: true
```

J'ai utilisé le plug-in de pagination PageHelper de Mybatis.

**Configuration de base:**

`UserServiceImp.java`

```java
    public PageBean findByPage(Goods goods, int pageCode, int pageSize) {
        // Utiliser le plugin de pagination Mybatis
        PageHelper.startPage(pageCode, pageSize);

        // Appelez la méthode de requête de pagination, en fait, consiste à interroger toutes les données, mybatis nous aide automatiquement à calculer la page
        Page<Goods> page = goodsMapper.findByPage(goods);

        return new PageBean(page.getTotal(), page.getResult());
    }
```

## Implémenter le téléchargement de fichier

Le téléchargement de fichiers SpringMVC est la seule chose à faire.

Comme le projet frontal de ce projet utilise la technologie ElementUI + Vue.JS, le didacticiel relatif au téléchargement et à l'écho de fichiers frontal se trouve donc sur ce blog: [SpringMVC + ElementUI permet de télécharger et d'afficher des images echo] (http://tycoding.cn/2018/08/05/vue-7/)

En plus du code, ici est également configuré dans application.yml:

```yaml
spring:
  servlet:
    multipart:
      max-file-size: 10Mb
      max-request-size: 100Mb
```

Ceci équivaut à la configuration XML de SpringMVC:

```xml
<bean id="multipartResolver" class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
        <property name="maxUploadSize" value="500000"/>
</bean>
```

## Utilisation de la programmation d'aspect Spring AOP pour implémenter un intercepteur de connexion simple

Pour ce projet, nous n'intégrons pas les cadres de sécurité de Shiro et Spring Security, et nous utilisons la programmation d'aspect Spring AOP pour implémenter une interception de connexion simple:

```java
@Component
@Aspect
public class MyInterceptor {

    @Pointcut("within (cn.tycoding.controller..*) && !within(cn.tycoding.controller.admin.LoginController)")
    public void pointCut() {
    }
    @Around("pointCut()")
    public Object trackInfo(ProceedingJoinPoint joinPoint) throws Throwable {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) {
            attributes.getResponse().sendRedirect("/login"); // transfert manuel vers le chemin de mappage/login
        }
        return joinPoint.proceed();
    }
}
```

**Explication**

Veuillez vous référer à Baidu pour la programmation d'aspect de Spring AOP, ou vous pouvez également lire ce blog: [Spring AOP Thoughts] (http://tycoding.cn/2018/05/25/Spring-3/). Nous devons faire attention aux points suivants

1. Familiarisez-vous avec les expressions de ponctuation de AspectJ, où: `..*` indique toutes les méthodes et méthodes de sous-répertoires de son répertoire.

2. Si le blocage de la connexion est effectué, c'est-à-dire que les informations de connexion de l'utilisateur ne sont pas obtenues dans la session, il peut être nécessaire de transférer manuellement à la page `login`, où le mappage` login` est accédé.

3. Sur la base de 2, vous devez spécifier la valeur de retour de l'objet. Si le contrôleur AOP intercepté renvoie une adresse d'affichage, le contrôleur doit accéder à l'adresse de la vue, mais a été intercepté par AOP. Le contrôleur d'origine effectue alors le retour, mais L'adresse d'affichage n'a pas pu trouver 404.

4. N'oubliez pas d'appeler la méthode submit (), encontrez (): exécutez la méthode notifiée. Si elle n'est pas appelée, elle empêchera l'appel de la méthode notifiée, ce qui entraînera un retour de 404 dans le contrôleur.


# Aperçu

![](README/1.png)

![](README/2.png)

![](README/3.png)

![](README/4.png)


<br/>

# Échange

Si vous êtes intéressé, bienvenue dans mon groupe de communication Java: 671017003 et apprenez la technologie Java ensemble. Les blogueurs étudient eux-mêmes JAVA. La technologie est limitée. Si vous le pouvez, vous ferez de votre mieux pour fournir de l'aide ou des méthodes d'apprentissage. Bien sûr, le groupe répondra activement aux questions du novice. Alors n'hésitez pas, venez nous rejoindre!

<br/>

# Contact

Si vous avez des questions après avoir vu cet article, vous pouvez me contacter ou vous pouvez trouver des informations en cliquant sur ces liens.

- [LinkedIn @houssembenali](https://www.linkedin.com/in/houssembenali/)
- [GitLab@hoos](https://gitlab.com/houssembenali90)
- [Facebook@Hoosbenali](https://www.facebook.com/HOOS.benali)

