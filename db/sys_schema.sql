-- CREATE DATABASE springboot DEFAULT CHARACTER SET utf8;

DROP TABLE IF EXISTS tb_user;
DROP TABLE IF EXISTS tb_goods;

-- user table
CREATE TABLE tb_user(
  id BIGINT AUTO_INCREMENT COMMENT 'Numbering',
  username VARCHAR(100) COMMENT 'username',
  password VARCHAR(100) COMMENT 'password',
  CONSTRAINT pk_sys_user PRIMARY KEY(id)
) CHARSET=utf8 ENGINE=InnoDB;

INSERT INTO tb_user VALUES(1, 'hoos', '123');
INSERT INTO tb_user VALUES(2, 'user', '123');

-- Product table
CREATE TABLE tb_goods(
  id BIGINT AUTO_INCREMENT COMMENT 'Numbering',
  title VARCHAR(1000) COMMENT 'Product title',
  price VARCHAR(100) COMMENT 'Commodity price',
  image VARCHAR(1000) COMMENT 'product picture',
  brand VARCHAR(100) COMMENT 'product brand',
  CONSTRAINT pk_sys_user PRIMARY KEY(id)
) CHARSET=utf8 ENGINE=InnoDB;
