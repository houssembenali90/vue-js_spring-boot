package cn.tycoding.controller.admin;

import cn.tycoding.entity.Goods;
import cn.tycoding.entity.PageBean;
import cn.tycoding.entity.Result;
import cn.tycoding.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @auther Hoos
 * @date 2018/9/19
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * Paging query
     *
     * @param goods    Query conditions
     * @param pageCode current page
     * @param pageSize Number of records displayed per page
     * @return
     */
    @RequestMapping("/findByConPage")
    public PageBean findByConPage(Goods goods,
                                  @RequestParam(value = "pageCode", required = false) int pageCode,
                                  @RequestParam(value = "pageSize", required = false) int pageSize) {
        return goodsService.findByPage(goods, pageCode, pageSize);
    }

    /**
     * Add item
     *
     * @param goods
     * @return
     */
    @RequestMapping("/create")
    public Result create(@RequestBody Goods goods) {
        try {
            goodsService.create(goods);
            return new Result(true, "Created successfully");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "Unknown error has occurred");
        }
    }

    /**
     * Update data successfully
     *
     * @param goods
     * @return
     */
    @RequestMapping("/update")
    public Result update(@RequestBody Goods goods) {
        try {
            goodsService.update(goods);
            return new Result(true, "Update data successfully");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "Unknown error has occurred");
        }
    }

    /**
     * Delete data in bulk
     *
     * @param ids
     * @return
     */
    @RequestMapping("/delete")
    public Result delete(@RequestBody Long... ids) {
        try {
            goodsService.delete(ids);
            return new Result(true, "Update data successfully");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "Unknown error has occurred");
        }
    }

    /**
     * Query based on id
     *
     * @param id
     * @return
     */
    @RequestMapping("/findById")
    public List<Goods> findById(@RequestParam(value = "id", required = false) Long id) {
        return goodsService.findById(id);
    }

}
