//Set the global form submission format
Vue.http.options.emulateJSON = true;

// Vue instance
new Vue({
    el: '#app',
    data() {
        return {
            checked: false,
            register: {
                username: '',
                password: '',
                repassword: ''
            }
        }
    },
    methods: {
        submitForm(register) {
            if(this.register.repassword != this.register.password){
                // Popup error message box
                this.$emit(
                    'submit-form',
                    this.$message({
                        message: 'The passwords entered twice are different',
                        type: 'warning',
                        duration: 6000
                    }),
                );
                // Clear form status
                this.$refs[register].resetFields();
            }else{
                this.$refs[register].validate((valid) => {
                    if (valid) {
                        //submit Form
                        this.$http.post('/register', {
                            username: this.register.username,
                            password: this.register.password,
                        }).then(result => {
                            console.log(result);
                            // Determine whether the user successfully logs in, the backend returns JSON format data, otherwise the data is not available.
                            if (result.body.success) {
                                window.location.href = "/index";
                            } else {
                                // Popup error message box
                                this.$emit(
                                    'submit-form',
                                    this.$message({
                                        message: result.body.message,
                                        type: 'warning',
                                        duration: 6000
                                    }),
                                );
                                // Clear form status
                                this.$refs[register].resetFields();
                            }
                        });
                    } else {
                        this.$emit(
                            'submit-form',
                            this.$message({
                                message: 'Clear form status！',
                                type: 'warning',
                                duration: 6000
                            }),
                        );
                        return false;
                    }
                });
            }
        },
        registerEnter(register){
            this.submitForm(register);
        }
    }
});
