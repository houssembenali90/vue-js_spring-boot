package cn.tycoding.interceptor;

import cn.tycoding.entity.User;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Custom interceptor for simple login interception
 *
 * @auther Hoos
 * @date 2018/9/29
 */
@Component
@Aspect
public class MyInterceptor {

    @Pointcut("within (cn.tycoding.controller..*) && !within(cn.tycoding.controller.admin.LoginController)")
    public void pointCut() {

    }

    @Around("pointCut()")
    public Object trackInfo(ProceedingJoinPoint joinPoint) throws Throwable {

        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
//        request.getSession().setAttribute("user", new User()); //Test, manually add the user login session
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) {
            System.out.println("-----------User not logged in-----------");
            attributes.getResponse().sendRedirect("/login"); //Manually forward to the /login mapping path
        }
        System.out.println("-----------User is logged in-----------");

        //Must specify the Object return value, if the AOP intercepted Controller returns a view address, then the Controller should jump to the view address, but was intercepted by AOP, then the original Controller will still perform the return, but the view address can not find To 404
        //Remember to call the proceed() method
        //Proceed(): executes the notified method. If it is not called, it will block the calling of the notified method, which will cause the return in the Controller to be 404.
        return joinPoint.proceed();
    }


}
