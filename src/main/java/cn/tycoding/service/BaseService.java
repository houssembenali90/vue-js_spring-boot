package cn.tycoding.service;

import java.util.List;

/**
 * Generic Service Layer Interface
 *
 * @auther TyCoding
 * @date 2018/9/28
 */
public interface BaseService<T> {

    /**
     * Query all
     *
     * @return
     */
    List<T> findAll();

    /**
     * Query by ID
     *
     * @param id
     * @return
     */
    List<T> findById(Long id);

    /**
     * Add to
     *
     * @param t
     */
    void create(T t);

    /**
     * Delete (batch)
     *
     * @param ids
     */
    void delete(Long... ids);

    /**
     * update
     *
     * @param t
     */
    void update(T t);
}
