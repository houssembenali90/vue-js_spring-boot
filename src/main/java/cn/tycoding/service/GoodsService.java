package cn.tycoding.service;

import cn.tycoding.entity.Goods;
import cn.tycoding.entity.PageBean;

/**
 * @auther Hoos
 * @date 2018/9/19
 */
public interface GoodsService extends BaseService<Goods> {

    /**
     * Paging query
     * @param goods Query conditions
     * @param pageCode current page
     * @param pageSize Number of records per page
     * @return
     */
    PageBean findByPage(Goods goods, int pageCode, int pageSize);
}
