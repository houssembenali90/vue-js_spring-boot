package cn.tycoding.entity;

import java.io.Serializable;

/**
 * @auther Hoos
 * @date 2018/9/28
 */
public class Result implements Serializable {

    //critical result
    private boolean success;
    //returned messages
    private String message;

    public Result() {
    }

    public Result(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
