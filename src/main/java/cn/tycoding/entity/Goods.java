package cn.tycoding.entity;

import java.io.Serializable;

/**
 * Product table
 * @auther Hoos
 * @date 2018/9/29
 */
public class Goods implements Serializable {

    private Long id; //Product Number
    private String title; //Product title
    private String price; //Commodity price
    private String image; //product picture
    private String brand; //product brand

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
