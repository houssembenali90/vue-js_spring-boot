package cn.tycoding.service.impl;

import cn.tycoding.entity.Goods;
import cn.tycoding.entity.PageBean;
import cn.tycoding.mapper.GoodsMapper;
import cn.tycoding.service.GoodsService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @auther Hoos
 * @date 2018/9/19
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public List<Goods> findAll() {
        return null;
    }

    @Override
    public List<Goods> findById(Long id) {
        return goodsMapper.findById(id);
    }

    @Override
    public void create(Goods goods) {
        goodsMapper.create(goods);
    }

    @Override
    public void update(Goods goods) {
        goodsMapper.update(goods);
    }

    @Override
    public void delete(Long... ids) {
        for (Long id : ids) {
            goodsMapper.delete(id);
        }
    }

    /**
     * Paging query - conditional query method
     *
     * @param goods    Query conditions
     * @param pageCode current page
     * @param pageSize Number of records per page
     * @return
     */
    public PageBean findByPage(Goods goods, int pageCode, int pageSize) {
        //Use the Mybatis paging plugin
        PageHelper.startPage(pageCode, pageSize);

        //Calling the paged query method is actually querying all the data, mybatis automatically helps us to perform paged calculations.
        Page<Goods> page = goodsMapper.findByPage(goods);

        return new PageBean(page.getTotal(), page.getResult());
    }
}
