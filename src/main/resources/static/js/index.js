//Set the global form submission format
// Vue.http.options.emulateJSON = true;

//Vue instance
new Vue({
    el: '#app',
    data() {
        return {
            name: '',

            activeIndex: '1'
        }
    },
    methods: {
        //Get the username of the user login from vuex
        getName(){
            this.name = sessionStorage.getItem("name");
        }
    },
    //Declare cycle hook function --> execute after data and methods are rendered
    created() {
        this.getName(); //Get user login name
    }
});