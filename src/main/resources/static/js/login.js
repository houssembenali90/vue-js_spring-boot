//Set the global form submission format
Vue.http.options.emulateJSON = true;

// Vue instance
new Vue({
    el: '#app',
    data() {
        return {
            checked: false,
            login: {
                username: '',
                password: '',
                remember: ''
            },
            flag: true,
            loading: {}, //loading animation
        };
    },
    methods: {
        /**
         * Loading animation
         */
        loadings(){
            this.loading = this.$loading({
                lock: true,
                text: 'Desperately loading',
                spinner: 'el-icon-loading',
            });
            setTimeout(() => {
                this.loading.close();
            }, 2000);
        },

        submitForm(login) {
            this.$refs[login].validate((valid) => {
                if (valid) {
                    this.loadings(); //Loading animation
                    //submit Form
                    this.$http.post('/login', {
                        username: this.login.username,
                        password: this.login.password,
                    }).then(result => {
                        // Determine whether the user successfully logs in, the backend returns JSON format data, otherwise the data is not available.
                        if (result.body.success) {
                            // sessionStorage.setItem("name", this.login.name);
                            // sessionStorage.setItem("token", this.login);
                            // console.log(this.$store);
                            // this.$store.dispatch("token", this.login);

                            window.location.href = "/index";
                            this.loading.close(); //Turn off animation loading
                        } else {
                            // Popup error message box
                            this.$emit(
                                'submit-form',
                                this.$message({
                                    message: 'wrong user name or password！',
                                    type: 'warning',
                                    duration: 6000
                                }),
                            );
                            // Clear form status
                            this.$refs[login].resetFields();
                        }
                    });
                } else {
                    this.$emit(
                        'submit-form',
                        this.$message({
                            message: 'Incorrect input information！',
                            type: 'warning',
                            duration: 6000
                        }),
                    );
                    return false;
                }
            });
        },
        loginEnter(login){
            this.submitForm(login);
        },

    }
});